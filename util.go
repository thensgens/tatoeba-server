package main

import "strings"

func empty(s string) bool {
	return len(strings.TrimSpace(s)) == 0
}
