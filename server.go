package main

import (
	"context"
	"flag"
	"gitlab.com/thensgens/tatoeba"
	"gitlab.com/thensgens/tatoeba/api"
	"google.golang.org/grpc"
	"log"
	"net"
	"os"
)

var (
	fileName = getFilename()
	data     = tatoeba.NewInMem(fileName)
)

// This type implements the SearchSentenceServiceServer provided by the generated (proto) files.
type SentenceServer struct{}

func (s SentenceServer) GetSentence(c context.Context, r *api.GetRequest) (*api.GetResponse, error) {
	sentences, err := data.Get(r.Search)
	if err != nil {
		return nil, err
	}
	resp := &api.GetResponse{Sentences: sentences}
	return resp, nil
}

// This function currently reads the filename from a command-line argument.
func getFilename() string {
	fn := os.Args[1]
	if empty(fn) {
		log.Fatal("no filename provided. exiting..")
	}
	return fn
}

func main() {
	flag.Parse()
	lis, err := net.Listen("tcp", ":4545")
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	grpcServer := grpc.NewServer()
	api.RegisterSearchSentenceServiceServer(grpcServer, SentenceServer{})
	grpcServer.Serve(lis)
}
